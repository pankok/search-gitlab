import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:search_gitlab/modules/search/domain/entities/result_search.dart';
import 'package:search_gitlab/modules/search/domain/errors/errors.dart';
import 'package:search_gitlab/modules/search/domain/repositories/search_repository_interface.dart';
import 'package:search_gitlab/modules/search/domain/use_cases/search_by_text.dart';

import 'search_by_text_test.mocks.dart';

@GenerateMocks([ISearchRepository])
main() {
  final repository = MockISearchRepository();
  final useCase = SearchByText(repository);

  test("deve retornar uma lista de resultSearch", () async {
    when(repository.search('ola marilene')).thenAnswer((_) async {
      return Future.value(Right(<ResultSearch>[]));
    });
    final result = await useCase("ola marilene");
    expect(result.isRight(), true);
    expect(result.fold(id, id), isA<List<ResultSearch>>());
  });
  test("retornar erro quando lenght do texto for menor que 4", () async {
    when(repository.search('')).thenAnswer((_) async {
      return Future.value(Right(<ResultSearch>[]));
    });
    final result = await useCase('Ola');
    expect(result.isRight(), false);
    expect(result.fold(id, id), isA<FailureSearch>());
  });
}
