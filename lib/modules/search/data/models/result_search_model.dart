import 'package:search_gitlab/modules/search/domain/entities/result_search.dart';

class ResultSearchModel extends ResultSearch {
  final String title;
  final String subtitle;

  ResultSearchModel({required this.title, required this.subtitle})
      : super(title: title, subtitle: subtitle);

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'subtitle': subtitle,
    };
  }

  factory ResultSearchModel.fromMap(Map<String, dynamic> map) {
    return ResultSearchModel(
      title: map['title'],
      subtitle: map['subtitle'],
    );
  }
}
