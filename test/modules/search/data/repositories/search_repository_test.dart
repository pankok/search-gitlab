import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:search_gitlab/modules/search/data/datasources/search_datasource.dart';
import 'package:search_gitlab/modules/search/data/models/result_search_model.dart';
import 'package:search_gitlab/modules/search/data/repositories/search_repository.dart';
import 'package:search_gitlab/modules/search/domain/errors/errors.dart';

import 'search_repository_test.mocks.dart';

@GenerateMocks([ISearchDatasource])
main() {
  final dataSource = MockISearchDatasource();
  final repository = SearchRepository(dataSource);

  test("deve retornar uma lista de resultSearchModel", () async {
    when(dataSource.getSearch("lookfish"))
        .thenAnswer((_) async => <ResultSearchModel>[]);
    final result = await repository.search("lookfish");
    expect(result.fold(id, id), isA<List<ResultSearchModel>>());
  });

  test("retornar DataSourceErro caso o dataSource falhar", () async {
    when(dataSource.getSearch("lookfish")).thenThrow(Exception());
    final result = await repository.search("lookfish");
    expect(result.fold(id, id), isA<DataSourceErro>());
  });
}
