import 'package:dartz/dartz.dart';
import 'package:search_gitlab/modules/search/domain/entities/result_search.dart';
import 'package:search_gitlab/modules/search/domain/errors/errors.dart';
import 'package:search_gitlab/modules/search/domain/repositories/search_repository_interface.dart';

abstract class ISearchByText {
  Future<Either<FailureSearch, List<ResultSearch>>> call(String searchText);
}

class SearchByText implements ISearchByText {
  final ISearchRepository searchRepository;

  SearchByText(this.searchRepository);

  @override
  Future<Either<FailureSearch, List<ResultSearch>>> call(
      String searchText) async {
    if (searchText.length < 4) return Left(InvalidTextErro());
    return searchRepository.search(searchText);
  }
}
