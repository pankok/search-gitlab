class ResultSearch {
  final String title;
  final String subtitle;

  ResultSearch({required this.title, required this.subtitle});
}
