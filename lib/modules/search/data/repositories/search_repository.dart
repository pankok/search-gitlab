import 'package:search_gitlab/modules/search/data/datasources/search_datasource.dart';
import 'package:search_gitlab/modules/search/domain/entities/result_search.dart';
import 'package:dartz/dartz.dart';
import 'package:search_gitlab/modules/search/domain/errors/errors.dart';
import 'package:search_gitlab/modules/search/domain/repositories/search_repository_interface.dart';

class SearchRepository implements ISearchRepository {
  final ISearchDatasource dataSource;

  SearchRepository(this.dataSource);

  @override
  Future<Either<FailureSearch, List<ResultSearch>>> search(
      String searchText) async {
    try {
      final result = await dataSource.getSearch(searchText);
      return Right(result);
    } catch (e) {
      return Left(DataSourceErro());
    }
  }
}
